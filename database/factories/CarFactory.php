<?php

namespace Database\Factories;

use App\Models\Brand;
use App\Models\Car;
use App\Models\CarModel;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Car>
 */
class CarFactory extends Factory
{
    protected $model = Car::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'brand_id' => Brand::factory(),
            'car_model_id' => CarModel::factory(),
            'year' => $this->faker->year,
            'mileage' => $this->faker->numberBetween(0, 200000),
            'color' => $this->faker->safeColorName,
            'user_id' => User::factory(),
        ];
    }
}
