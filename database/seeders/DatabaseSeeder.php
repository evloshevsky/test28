<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Brand;
use App\Models\Car;
use App\Models\CarModel;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        // Создаем пользователей
        User::factory(10)->create()->each(function ($user) {
            // Создаем бренды для каждого пользователя
            $brands = Brand::factory(5)->create();

            // Для каждого бренда создаем модели
            $brands->each(function ($brand) use ($user) {
                $carModels = CarModel::factory(3)->create(['brand_id' => $brand->id]);
                // Для каждой модели создаем автомобили
                $carModels->each(function ($carModel) use ($user, $brand) {
                    Car::factory(2)->create([
                        'brand_id' => $brand->id,
                        'car_model_id' => $carModel->id,
                        'user_id' => $user->id,
                    ]);
                });
            });
        });
    }
}
