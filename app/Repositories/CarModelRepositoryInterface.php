<?php

namespace App\Repositories;

interface CarModelRepositoryInterface
{
    public function all();
}
