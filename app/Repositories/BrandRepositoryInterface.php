<?php

namespace App\Repositories;

interface BrandRepositoryInterface
{
    public function all();
}
