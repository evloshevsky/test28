<?php

namespace App\Repositories;

use App\Models\Brand;

class BrandRepository implements BrandRepositoryInterface
{
    public function all(): \Illuminate\Database\Eloquent\Collection
    {
        return Brand::all();
    }
}
