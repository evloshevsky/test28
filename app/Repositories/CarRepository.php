<?php

namespace App\Repositories;

use App\Models\Car;

class CarRepository implements CarRepositoryInterface
{
    public function all(): \Illuminate\Database\Eloquent\Collection
    {
        return Car::all();
    }

    public function findByUser(int $userId)
    {
        return Car::where('user_id', $userId)->get();
    }

    public function create(array $data)
    {
        return Car::create($data);
    }

    public function update(int $id, array $data)
    {
        $car = Car::findOrFail($id);
        $car->update($data);

        return $car;
    }

    public function delete(int $id): void
    {
        Car::destroy($id);
    }
}
