<?php

namespace App\Repositories;

use App\Models\CarModel;

class CarModelRepository implements CarModelRepositoryInterface
{
    public function all(): \Illuminate\Database\Eloquent\Collection
    {
        return CarModel::all();
    }
}
