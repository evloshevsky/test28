<?php

namespace App\Services;

use App\Repositories\CarModelRepositoryInterface;

class CarModelService
{
    protected CarModelRepositoryInterface $carModelRepository;

    public function __construct(CarModelRepositoryInterface $carModelRepository)
    {
        $this->carModelRepository = $carModelRepository;
    }

    public function getAllCarModels()
    {
        return $this->carModelRepository->all();
    }
}
