<?php

namespace App\Services;

use App\Repositories\CarRepositoryInterface;

class CarService
{
    protected CarRepositoryInterface $carRepository;

    public function __construct(CarRepositoryInterface $carRepository)
    {
        $this->carRepository = $carRepository;
    }

    public function getAllCars()
    {
        return $this->carRepository->all();
    }

    public function getAllCarsByUser($userId)
    {
        return $this->carRepository->findByUser($userId);
    }

    public function createCar(array $data)
    {
        return $this->carRepository->create($data);
    }

    public function updateCar(int $id, array $data)
    {
        return $this->carRepository->update($id, $data);
    }

    public function deleteCar(int $id): void
    {
        $this->carRepository->delete($id);
    }
}
