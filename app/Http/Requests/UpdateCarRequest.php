<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCarRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'brand_id' => 'sometimes|required|exists:brands,id',
            'car_model_id' => 'sometimes|required|exists:car_models,id',
            'year' => 'nullable|integer',
            'mileage' => 'nullable|integer',
            'color' => 'nullable|string',
            'user_id' => 'nullable|exists:users,id'
        ];
    }
}
