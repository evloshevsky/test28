<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCarRequest;
use App\Http\Requests\UpdateCarRequest;
use App\Services\CarService;
use Illuminate\Http\Request;

class CarController extends Controller
{
    protected $carService;

    public function __construct(CarService $carService)
    {
        $this->carService = $carService;
    }

    public function index(Request $request)
    {
        $cars = $this->carService->getAllCarsByUser($request->user()->id);

        return response()->json($cars);
    }

    public function store(StoreCarRequest $request)
    {
        $data = $request->validated();
        $data['user_id'] = $request->user()->id;

        $car = $this->carService->createCar($data);

        return response()->json($car, 201);
    }

    public function update(UpdateCarRequest $request, $id)
    {
        $car = Car::where('id', $id)->where('user_id', $request->user()->id)->firstOrFail();

        $updatedCar = $this->carService->updateCar($id, $request->validated());

        return response()->json($updatedCar);
    }

    public function destroy(Request $request, $id)
    {
        $car = Car::where('id', $id)->where('user_id', $request->user()->id)->firstOrFail();
        $this->carService->deleteCar($id);

        return response()->json(null, 204);
    }
}
