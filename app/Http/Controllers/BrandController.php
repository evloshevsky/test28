<?php

namespace App\Http\Controllers;

use App\Services\BrandService;

class BrandController extends Controller
{
    protected $brandService;

    public function __construct(BrandService $brandService)
    {
        $this->brandService = $brandService;
    }

    public function index()
    {
        return response()->json($this->brandService->getAllBrands());
    }
}
