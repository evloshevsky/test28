<?php

namespace App\Http\Controllers;

use App\Services\CarModelService;

class CarModelController extends Controller
{
    protected $carModelService;

    public function __construct(CarModelService $carModelService)
    {
        $this->carModelService = $carModelService;
    }

    public function index()
    {
        return response()->json($this->carModelService->getAllCarModels());
    }
}
